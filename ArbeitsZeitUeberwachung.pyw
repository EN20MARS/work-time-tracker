# Work Time Tracker
# Copyright (C) 2021  Enzo Marscheck
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import json
import psutil
import time
from datetime import date
import PySimpleGUI as sg
import datetime as dt
import bisect

currentBootTStamp = psutil.boot_time()
currentBootDate = date.fromtimestamp(currentBootTStamp)
currentBootTStampHR = dt.datetime.fromtimestamp(currentBootTStamp).strftime("%H:%M")
execute = True
notifyBreak = False
halveDailyBreakInSec = 20 * 60
workDayInSec = 496 * 60
lunchReminder = True
trackFlextime = False
reminderFlags = []
reminders = []
flextimeBalance = 0
skipPause = False
currentSnoozeTime = 0
defaultSnooze = 5

def LoadConf():
  with open("AZUConfig.json","r") as conf:
    global halveDailyBreakInSec, workDayInSec, flextimeBalance, reminders, config, lunchReminder, trackFlextime, defaultSnooze
    
    config = json.load(conf)
    hoursPerDay = (config["hoursPerWeek"]/config["workDaysPerWeek"])
    workDayInMin = (hoursPerDay * 60) + config["dailyBreakInMin"] - config["flextimeBalance"]
    halveDailyBreakInSec = (config["dailyBreakInMin"]/2) * 60
    workDayInSec = workDayInMin * 60
    reminders = config["reminders"]
    lunchReminder = config["lunchReminder"]
    trackFlextime = config["trackFlextime"]
    defaultSnooze = config["defaultSnooze"]
  conf.close()

LoadConf()

sg.theme('DarkGrey11')

progresBarRow = [sg.Text("mmm", k="workDur"), sg.ProgressBar(workDayInSec, expand_x=True, bar_color=('green', 'red'), k="bar",size=(0, 10)), sg.Text("mmm", k="leftWorkDur")]

col1 = sg.Column([[sg.Text(text="Start time: hh:mm",  justification='center', key="startTKey")]], element_justification='center', justification='center', expand_x=True)
col2 = sg.Column([[sg.Text(text="Lunch time: hh:mm",  justification='center', key="lunchTKey")]], element_justification='center', justification='center', expand_x=True)
col3 = sg.Column([[sg.Text(text="End time: hh:mm",    justification='center', key="endTKey")]],   element_justification='center', justification='center', expand_x=True)
statusLine = [col1, col2, col3]

remCol1 = sg.Column([[sg.Listbox(reminders, expand_x=True, expand_y=True, key='reminders')]], element_justification='left', justification='left', expand_x=True, expand_y=True)
remCol2 = sg.Column([[sg.Text("", expand_y=True, expand_x=True)],
                    [sg.Spin(list(range(1, 1441)), s=(7, 1), initial_value=15, key="addreminder")],
                    [sg.Button(button_text="Add", s=(7,1))], [sg.Button(button_text="Remove", s=(7,1))]],
                    element_justification='right', justification='right')
tab1Line = [remCol1, remCol2]
tab1 = sg.Tab("Reminders:", [tab1Line], expand_y=True)

confCol1 = sg.Column([[sg.Text(text="General worktime:")],
                      [sg.Spin(list(range(1, 61)),    initial_value=config["hoursPerWeek"],     s=(4,1), k="hoursPerWeek"),   sg.Text(text="Weekly hours")],
                      [sg.Spin(list(range(1, 8)),     initial_value=config["workDaysPerWeek"],  s=(4,1), k="daysPerWeek"),    sg.Text(text="Days/Week")],
                      [sg.Spin(list(range(0, 1441)),  initial_value=config["dailyBreakInMin"],  s=(4,1), k="dailyBreak"),     sg.Text(text="Daily break [min]")],
                      [sg.Spin(list(range(1, 1001)),  initial_value=config["defaultSnooze"],    s=(4,1), k="defaultSnooze"),  sg.Text(text="Snooze duration")]], expand_x=True)
confCol2 = sg.Column([[sg.Text(text="Flextime balance:")],
                      [sg.Radio("+", 1, default=(config["flextimeBalance"]>=0), k="flexSign+"), sg.Radio("-", 1, default=(config["flextimeBalance"]<0), k="flexSign-")],
                      [sg.Spin(list(range(0, 1001)),  initial_value=int(abs(config["flextimeBalance"])/60), s=(4,1), k="flexHours"),    sg.Text(text="Hours")],
                      [sg.Spin(list(range(0, 60)),    initial_value=abs(config["flextimeBalance"])%60,      s=(4,1), k="flexMinutes"),  sg.Text(text="Minutes")]], expand_x=True)
confCol3 = sg.Column([[sg.Checkbox("Lunch reminder", default=lunchReminder, k="lunchReminder")],
                      [sg.Checkbox("Track flextime", default=trackFlextime, k="trackFlextime")],
                      [sg.Button(button_text="Load", expand_x=True)],
                      [sg.Button(button_text="Save", expand_x=True)]])

tab2Line = [confCol1, sg.VerticalSeparator(), confCol2, sg.VerticalSeparator(), confCol3]
tab2 = sg.Tab("Config:", [tab2Line], expand_y=True)

othCol1 = sg.Column([[sg.Text("")]],  expand_x=True)
othCol2 = sg.Column([[sg.Text("", expand_y=True)], [sg.Text("", expand_y=True)], [sg.Button(button_text="Restart", expand_x=True)],[sg.Button(button_text="Close", expand_x=True)]])
tab3Line = [othCol1, othCol2]
tab3 = sg.Tab("Other:", [tab3Line])
confTabGroup = [sg.TabGroup([[tab1, tab2, tab3]], expand_x=True, expand_y=True)]

confLayout = [statusLine, progresBarRow, confTabGroup]
configWindow = sg.Window("Work Time Tracker", confLayout, resizable=True, use_custom_titlebar=False, modal=True , disable_close=True, finalize=True)
configWindow.set_min_size(configWindow.size)

def blinkNotify(msg):
  global currentSnoozeTime, execute
  layout = [[sg.Text(msg, size=(80,1), font="font 40", justification="center", k="blinkText", expand_y=True)], [sg.Spin(list(range(1, 1001)), initial_value=defaultSnooze, auto_size_text=True,k="snoozeSpin"), sg.Button("Snooze"), sg.Button("Stop")]]
  
  w, h = sg.Window.get_screen_size()
  window = sg.Window("Work Time Tracker", layout, element_justification='c', use_custom_titlebar = True, modal=True ,size=(int(w/2),int(h/2)), location=(int(w/4),int(h/4)), keep_on_top=True, finalize=True)
  
  toggle = False
  while True:
    event, values = window.read(500)
    window["blinkText"].update(visible = toggle)
    toggle = not toggle
    if event == sg.WIN_CLOSED:
      break
    elif event == "Snooze":
      currentSnoozeTime += values["snoozeSpin"]
      break
    elif event == "Stop":
      execute = False
      break
  
  window.close()

def notify(msg):
  layout = [[sg.Text(msg, size=(80,1), font="font 40", justification="center", expand_y=True)], [sg.Button("OK")]]
  
  w, h = sg.Window.get_screen_size()
  window = sg.Window("Work Time Tracker", layout, element_justification='c', use_custom_titlebar = True, modal=True ,size=(int(w/2),int(h/2)), location=(int(w/4),int(h/4)), keep_on_top=True, finalize=True)
  
  while True:
    event, values = window.read()
    if event == "OK" or event == sg.WIN_CLOSED:
      break
  
  window.close()

def LoadTStamps():
  with open("AZUTStamps.json","r") as stamps:
    tmpStamps=json.load(stamps)
    stamps.close()
  return tmpStamps["bootTimeStamp"], date.fromisoformat(tmpStamps["bootDate"])

def UpdateTStamps():
  global bootTStamp, bootDate
  with open("AZUTStamps.json","w") as stamps:
    currentStampsDict = {'bootTimeStamp':currentBootTStamp, 'bootDate':currentBootDate, 'bootTimeHR':currentBootTStampHR}
    json.dump(currentStampsDict, stamps, indent = 2, sort_keys = True, default = str)
  stamps.close()
  bootTStamp, bootDate = LoadTStamps()

def EvalWorkTime():
  global notifyBreak
  workDurationInSec = (time.time() - int(bootTStamp)) + flextimeBalance
  workDuration = (workDurationInSec / 60)
  configWindow["bar"].update(workDurationInSec, max = workDayInSec)
  configWindow['workDur'].update(str(int(workDuration)))
  configWindow['leftWorkDur'].update(str(int((workDayInSec/60)-workDuration)))
  if lunchReminder & (not notifyBreak):
    breakTime = (workDayInSec/2 - halveDailyBreakInSec) / 60
    if workDuration > breakTime:
      notify("Mittagspause!!!")
      notifyBreak = True
  elif not all(reminderFlags):
    for n in range(len(reminderFlags)):
      if not reminderFlags[n]:
        reminderStamp = int(workDayInSec / 60) - reminders[n]
        if workDuration >= reminderStamp:
          msg = "Feierabend in " + str(reminders[n]) + " Minuten..."
          notify(msg)
          reminderFlags[n] = True
        break
  else:
    if workDuration > ((workDayInSec/60) + currentSnoozeTime):
      blinkNotify("Feierabend!!!")

def UpdateUI():
  global skipPause
  LoadConf()
  LoadTStamps()
  configWindow['startTKey'].update('Start time: ' + dt.datetime.fromtimestamp(bootTStamp).strftime("%H:%M"))
  configWindow['lunchTKey'].update('Lunch time: ' + dt.datetime.fromtimestamp(bootTStamp + (workDayInSec/2 - halveDailyBreakInSec)).strftime("%H:%M"))
  configWindow['endTKey'].update('End time: ' + dt.datetime.fromtimestamp(bootTStamp + workDayInSec).strftime("%H:%M"))
  skipPause = True

def renewReminderFlags():
  global reminderFlags
  reminderFlags = []
  for n in reminders:
    reminderFlags.append(False)

def saveReminders():
  with open("AZUConfig.json","r") as conf:
    tmpConf = json.load(conf)
    tmpConf["reminders"] = reminders
  with open("AZUConfig.json","w") as conf:
    json.dump(tmpConf, conf, indent = 2, sort_keys = True, default = str)
  renewReminderFlags()
  conf.close()

def addReminder(reminder):
  if isinstance(reminder, int):
    if not reminder in reminders:
      reminders.reverse()
      bisect.insort(reminders, reminder)
      reminders.reverse()
      configWindow['reminders'].update(reminders)
      saveReminders()
  else:
    notify('Reminder can not be before start time')

def removeReminder(reminder):
  if reminder:
    if reminder[0] in reminders:
      reminders.remove(reminder[0])
      configWindow['reminders'].update(reminders)
      saveReminders()
  else:
    notify('You need to select a reminder to remove it')

def loadConfigFile():
  with open("AZUConfig.json","r") as conf:
    tmpConf = json.load(conf)
    configWindow["dailyBreak"].update(value=tmpConf["dailyBreakInMin"])
    configWindow["hoursPerWeek"].update(value=tmpConf["hoursPerWeek"])
    configWindow["lunchReminder"].update(value=tmpConf["lunchReminder"])
    configWindow["trackFlextime"].update(value=tmpConf["trackFlextime"])
    configWindow["flexHours"].update(value=int(abs(tmpConf["flextimeBalance"])/60))
    configWindow["flexMinutes"].update(value=abs(tmpConf["flextimeBalance"])%60)
    configWindow["daysPerWeek"].update(value=tmpConf["workDaysPerWeek"])
    configWindow["flexSign+"].update(value=(tmpConf["flextimeBalance"]>=0))
    configWindow["flexSign-"].update(value=(tmpConf["flextimeBalance"]<0))
    configWindow["defaultSnooze"].update(value=(tmpConf["defaultSnooze"]))
  conf.close()
  UpdateUI()

def saveConfigFile(values):
  with open("AZUConfig.json","r") as conf:
    tmpConf = json.load(conf)
    tmpConf["dailyBreakInMin"] = values["dailyBreak"]
    tmpConf["hoursPerWeek"] = values["hoursPerWeek"]
    tmpConf["lunchReminder"] = values["lunchReminder"]
    tmpConf["trackFlextime"] = values["trackFlextime"]
    sign = 1
    if values["flexSign-"]:
      sign = -1
    tmpConf["flextimeBalance"] = sign * (values["flexMinutes"] + (values["flexHours"] * 60))
    tmpConf["workDaysPerWeek"] = values["daysPerWeek"]
    tmpConf["defaultSnooze"] = values["defaultSnooze"]
  with open("AZUConfig.json","w") as conf:
    json.dump(tmpConf, conf, indent = 2, sort_keys = True, default = str)
  conf.close()
  UpdateUI()

def confEventEval(event, values):
  global execute
  if event == 'Add':
    addReminder(values['addreminder'])
  elif event == 'Remove':
    removeReminder(values['reminders'])
  elif event == 'Save':
    saveConfigFile(values)
  elif event == 'Load':
    loadConfigFile()
  elif event == 'Close':
    execute = False

def main():
  global reminderFlags, bootTStamp, bootDate, confval, skipPause
  renewReminderFlags()
  
  bootTStamp, bootDate = LoadTStamps()
  LoadConf()
  configWindow['reminders'].update(reminders)
  UpdateUI()
  
  notify("Arbeitszeitüberwachung gestartet")
  startTime = time.time()
  
  while execute:
    bootTStamp, bootDate = LoadTStamps()
    
    if bootDate != currentBootDate:
      UpdateTStamps()
      UpdateUI()
      notify("Startzeit aktualisiert")
    
    LoadConf() #is this really nessecery every loopiteration
    EvalWorkTime()
    
    endTime = time.time()
    pause = 60 - (endTime - startTime)
    if (pause <= 0) or skipPause:
      skipPause = False
      pause = 1

    confevent, confval = configWindow.read(pause*1000)
    
    startTime = endTime
    
    if confevent != '__TIMEOUT__':
      confEventEval(confevent, confval)
  
  print("good by x_x")

if __name__ == "__main__":
  main()